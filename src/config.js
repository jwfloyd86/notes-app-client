export default {
  MAX_ATTACHMENT_SIZE: 5000000,

  s3: {
    REGION: "us-east-1",
    BUCKET: "bainova-notes-app-uploads"
  },
  apiGateway: {
    REGION: "us-east-1",
    URL: "https://lrokjr3442.execute-api.us-east-1.amazonaws.com/prod"
  },
  cognito: {
    REGION: "us-east-1",
    USER_POOL_ID: "us-east-1_mgJ2wXBYc",
    APP_CLIENT_ID: "5d0t9daitp6vvuf1tk5u0v9h27",
    IDENTITY_POOL_ID: "us-east-1:ebd8d62f-5129-44dc-8d3e-bd900b8088ae"
  }
};
